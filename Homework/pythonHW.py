from math import cos, sin, pi
import numpy as np

def QPE_Probability(theta,amplitude,t,y):       # use of special word "def"
                            # receives the parameter called "theta", a variable

    M = 2**t

    py = []
    for i in range(len(y)):
        py_result = 0
        term1 = 0
        term2 = 0
        for j in range(len(amplitude)):
            if y[i]-theta[j]*M == 0:
                term1 = term1 + amplitude[j].conjugate() * amplitude[j]
            else:
                term2 = term2 + ((amplitude[j].conjugate() * amplitude[j])/(M*M))*((1-cos((2*pi*(y[i]-theta[j]*M)*M)/M))/(1-cos((2*pi*(y[i]-theta[j]*M))/M)))
            py_result = py_result + term1 + term2
        py.append(py_result)


    return py

def QPE_DensityMatrix(theta,amplitude,t,y):
    return True




'''
from math import sqrt

t = 3
a = [sqrt(1/3), sqrt(1/3), sqrt(1/3)]
theta = [1/8, 3/8, 5/8]

yaux = range(80)
y = []
for i in range(len(yaux)):
    y.append(yaux[i]/10)

prob = QPEProbability(theta,a,t,y)
print(prob)
print(len(prob), len(y))
'''